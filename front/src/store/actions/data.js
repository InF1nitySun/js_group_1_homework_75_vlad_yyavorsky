import {FETCH_DATA, SEND_DECODE_DATA, SEND_ENCODE_DATA} from "./actionTypes";
import axios from '../../axios-api';

export const fetchData = field => {
    field.persist();
    return {type: FETCH_DATA, field};
};

export const setDecodeData = result => {
    return {type: SEND_DECODE_DATA, result};
};
export const setEncodeData = result => {
    return {type: SEND_ENCODE_DATA, result};
};

export const sendDecodeData = (e) => {
    e.preventDefault();
    return (dispatch, getState) => {

        if (!getState().password || !getState().decode) return null;
        return axios.post('/encode', {password: getState().password, message: getState().decode})
            .then(response => dispatch(setEncodeData(response.data)))
            .catch(err => console.log(err));
    }
};
export const sendEncodeData = (e) => {
    e.preventDefault();
    return (dispatch, getState) => {
        if (!getState().password || !getState().encode) return null;

        return axios.post('/decode', {password: getState().password, message: getState().encode})
            .then(response => dispatch(setDecodeData(response.data)))
            .catch(err => console.log(err));
    }
};
