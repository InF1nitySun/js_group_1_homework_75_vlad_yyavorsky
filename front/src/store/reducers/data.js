import {FETCH_DATA, SEND_DECODE_DATA, SEND_ENCODE_DATA} from "../actions/actionTypes";

const initialState = {
    password: '',
    encode: '',
    decode: ''
};

const reducer = (state = initialState, action) => {

    switch (action.type) {

        case FETCH_DATA:
            console.log(state, action.field.target.name, action.field.target.value);
            return {...state, [action.field.target.name]: action.field.target.value};

        case SEND_DECODE_DATA:
            return {...state, decode: action.result};

        case SEND_ENCODE_DATA:
            return {...state, encode: action.result};
        default:
            return state;
    }
};

export default reducer;