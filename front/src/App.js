import React, {Component} from "react";
import {connect} from "react-redux";
import {Button, Col, FormControl, FormGroup} from "react-bootstrap";
import {fetchData, sendDecodeData, sendEncodeData} from "./store/actions/data";

class App extends Component {

    render() {
        return (
            <div className="container">
                <h2>HomeWork75</h2>
                <form>
                    <FormGroup>
                        <Col sm={2}>Раскодировать</Col>
                        <Col sm={10}>
                            <FormControl
                                placeholder="Decode"
                                name="decode"
                                value={this.props.decode}
                                onChange={(e) => this.props.onChangeFormField(e)}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup>
                        <Col sm={2}>Пароль</Col>
                        <Col sm={8}>
                            <FormControl
                                type="password"
                                placeholder="Password"
                                name="password"
                                value={this.props.password}
                                onChange={(e) => this.props.onChangeFormField(e)}
                            />
                        </Col>
                    </FormGroup>
                    <Button type="submit"
                            onClick={(e) => this.props.decodeHandler(e, this.props.decode)}>Decode</Button>
                    <Button type="submit"
                            onClick={(e) => this.props.encodeHandler(e, this.props.encode)}>Encode</Button>

                    <FormGroup>
                        <Col sm={2}>Закодировать</Col>
                        <Col sm={10}>
                            <FormControl
                                placeholder="Encode"
                                name="encode"
                                value={this.props.encode}
                                onChange={(e) => this.props.onChangeFormField(e)}
                            />
                        </Col>
                    </FormGroup>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        password: state.password,
        encode: state.encode,
        decode: state.decode

    };
};

const mapDispatchToProps = dispatch => {
    return {
        onChangeFormField: (e) => dispatch(fetchData(e)),
        decodeHandler: (e) => dispatch(sendEncodeData(e)),
        encodeHandler: (e) => dispatch(sendDecodeData(e))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
