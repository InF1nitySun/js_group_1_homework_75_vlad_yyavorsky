const express = require('express');
const cors = require('cors');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const port = 8001;

app.use(express.json());
app.use(cors());


app.get('/', (req, res) => {

    res.send('InF1IP');

});

app.post('/encode/', (req, res) => {
    const product = req.body;

    const secret = product.password;
    const message = product.message;

    let answer = Vigenere.Cipher(secret).crypt(message);

    // console.log(answer);
    res.send(answer);

});

app.post('/decode/', (req, res) => {
    const product = req.body;

    const secret = product.password;
    const message = product.message;

    let answer = Vigenere.Decipher(secret).crypt(message);

    // console.log(answer);
    res.send(answer);
});


app.listen(port, () => {
    console.log('server onLine ' + port);
});
